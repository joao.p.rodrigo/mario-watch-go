module gitlab.com/joao.p.rodrigo/mario-watch-go

go 1.16

require (
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible // indirect
	github.com/golang-migrate/migrate/v4 v4.14.1 // indirect
	github.com/jmoiron/sqlx v1.3.4 // indirect
	github.com/mattn/go-sqlite3 v1.14.8 // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	go.mongodb.org/mongo-driver v1.7.3 // indirect
)

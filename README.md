# MarioWatch

## Project summary

Telegram bot notification service of Nintendo Switch game prices.

This is a proof of concept and a work in progress, intended for educational purposes.

The goal of this project is to get more confortable with golang
and a ports and adapters architecture, that separates the domain logic
from the implementation logic.

## Project details

You need to have a telegram bot created. [See this](https://core.telegram.org/bots)

Then run the service. It accepts 3 commands at the moment, described below. 
URLs are always nintendo's shop specific game URL.

**For now, only nintendo.pt games are allowed!**

`/game <url>`

Gets the details of the game specified by the URL.

`/addgame <url>`

Subscribe to be notified when the game's price drops.

`/removegame <url>`

Remove subscription of the game, so you don't get new notifications.

`/mygames` (not implemented yet)

Return the games the user is subscribed to.

More to come... eventually.

The service works by keeping a database of the searched and subscribed games.
At a specified interval, it polls the nintendo website for updates on the prices of the games.

If the price has lowered, then it finds all the subscribed users and notifies them via telegram.

## Database

Right now the database chosen is SQLite.

There are plans to make an adapter with Mongo and a pure golang (no C dependencies) database.

## Main Milestones

- [x] Subscribe to game flow
- [x] Remove subscription flow
- [x] Refactor game to have nsuID field
- [x] Price updates notifications
- [ ] Use context and goroutines for concurrency and cancellability
- [ ] Fix current tests and add tests to everything

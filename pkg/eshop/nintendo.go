// Package eshop provides a set of functionality to retrieve
// information about a game on an eshop website

package eshop

import (
	"errors"
	"fmt"
	"html"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"

	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/mario"
)

type WebsiteGame struct {
	mario.Game
}

type domainSetting struct {
	currency string
	region   string
	lang     string
}

type Website struct{}

var ErrorInvalidURL = errors.New("URL is Invalid")
var ErrorRegionNotFound = errors.New("this region is not supported")

var domainSettings = map[string]domainSetting{
	"nintendo.pt": {
		currency: "€",
		region:   "PT",
		lang:     "pt",
	},
}

var reName = regexp.MustCompile("<h1>(.*?)</h1>")

var netClient = http.Client{Timeout: time.Second * 10}

// New returns a new Game if the provided URL can be parsed
func New(url string) (*WebsiteGame, error) {
	g, err := newConfiguredGame(url)
	if err != nil {
		return nil, fmt.Errorf("new (WebsiteGame) %v: %w", url, ErrorInvalidURL)
	}
	data, err := g.fetchURLData()
	if err != nil {
		return nil, fmt.Errorf("new (WebsiteGame) %v: %w", url, err)
	}
	g.fetchName(data)
	err = g.fetchNsuID(data)
	if err != nil {
		return nil, fmt.Errorf("new (WebsiteGame) %v: %w", url, err)
	}
	err = g.fetchPrice()
	if err != nil {
		return nil, fmt.Errorf("new (WebsiteGame) %v: %w", url, err)
	}

	return g, nil
}

func newConfiguredGame(url string) (*WebsiteGame, error) {
	if len(url) < 3 {
		return nil, fmt.Errorf("newConfiguredGame %v: %w", url, ErrorInvalidURL)
	}

	url = stripURL(url)

	g := WebsiteGame{mario.Game{URL: url}}
	err := g.fetchDomainSettings()
	if err != nil {
		return nil, fmt.Errorf("newConfiguredGame %v: %w", url, err)
	}
	return &g, nil
}

// stripURL returns an URL without any query parameters.
func stripURL(url string) string {
	return strings.Split(url, "?")[0]
}

// NewGame crawls a URL to get game data and returns a mario domain Game struct
func (w *Website) NewGame(url string) (*mario.Game, error) {
	game, err := New(url)
	if err != nil {
		return nil, fmt.Errorf("NewGame: %w", err)
	}
	return &game.Game, nil
}

// GetPriceFromGame returns a mario.PricePoint for the given Game.
//
// It uses the game nsuID, region and language to make an API call, therefore it's faster than
// creating a new game and getting it's price, since it only calls the website once, instead of twice.
func (w *Website) GetPriceFromGame(game *mario.Game) (*mario.PricePoint, error) {
	price, err := getAPIPriceFromNsuID(game.NsuID, game.Region, game.Language)
	if err != nil {
		return nil, fmt.Errorf("GetPriceFromGame nsuid %v: %w", game.NsuID, err)
	}

	return &mario.PricePoint{
		Timestamp: time.Now(),
		Price:     price,
	}, nil
}

// fetchName parses the name of the game.
func (g *WebsiteGame) fetchName(websiteData []byte) {
	nameMatch := reName.FindSubmatch(websiteData)
	escapedName := string(nameMatch[1])
	unescapedName := html.UnescapeString(escapedName)
	g.Name = unescapedName
}

// fetchNsuID parses the nsuID (an id used to identify the game in the store) from the pagedata.
func (g *WebsiteGame) fetchNsuID(websiteData []byte) error {
	nsuID, err := getNsuID(websiteData)
	if err != nil {
		return fmt.Errorf("fetchNsuID %w", err)
	}
	g.NsuID = nsuID
	return nil
}

// fetchPrice parses the current game price of the game. It uses the nintendo eshop API for that.
func (g *WebsiteGame) fetchPrice() error {
	price, err := getAPIPriceFromNsuID(g.NsuID, g.Region, g.Language)
	if err != nil {
		return fmt.Errorf("fetchPrice: %w", err)
	}
	g.Price = price
	return nil
}

func (g *WebsiteGame) fetchDomainSettings() error {
	u, err := url.Parse(g.URL)
	if err != nil {
		return fmt.Errorf("fetchDomainSettings: can't parse url %v", g.URL)
	}

	parts := strings.Split(u.Hostname(), ".")
	if len(parts) < 2 {
		return fmt.Errorf("fetchDomainSettings: domain not found: %w", ErrorRegionNotFound)
	}
	domain := parts[len(parts)-2] + "." + parts[len(parts)-1]

	conf, ok := domainSettings[domain]
	if !ok {
		return ErrorRegionNotFound
	}
	g.Region = conf.region
	g.Language = conf.lang
	g.Currency = conf.currency

	return nil
}

func (g *WebsiteGame) fetchURLData() ([]byte, error) {
	return getURLData(g.URL)
}

func getURLData(url string) ([]byte, error) {
	resp, err := netClient.Get(url)
	if err != nil {
		return nil, fmt.Errorf("getURLData getting %v: %w", url, err)
	}
	defer resp.Body.Close()
	if (resp.StatusCode != 200) || (strings.ToLower(resp.Request.URL.Path) == "/notfound") {
		return nil, mario.ErrorGameURLNotAvailable
	}

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("getURLData reading URL %v: %w", url, err)
	}

	return data, nil
}

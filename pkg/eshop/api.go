package eshop

import (
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strconv"
)

// https://api.ec.nintendo.com/v1/price?country=PT&lang=pt&ids=70010000013703

type APIPriceContainer struct {
	Prices []APIPrices `json:"prices"`
}

type APIPrices struct {
	RegularPrice  APIPrice `json:"regular_price"`
	DiscountPrice APIPrice `json:"discount_price"`
}

type APIPrice struct {
	Amount   string `json:"amount"`
	Currency string `json:"currency"`
	RawValue string `json:"raw_value"`
}

var reNsuID = regexp.MustCompile(`"offdeviceNsuID": "([0-9]+)"`)

var ErrorNsuIDNotFound = errors.New("NsuID not found")
var ErrorPriceNotFound = errors.New("price not found")

func getAPIPriceFromNsuID(nsuID int64, country string, lang string) (float64, error) {
	container, err := getPriceAPIData(nsuID, country, lang)
	if err != nil {
		return 0, fmt.Errorf("getAPIPriceFromNsuID %w", err)
	}

	if len(container.Prices) == 0 {
		return 0, fmt.Errorf("getAPIPriceFromNsuID %w", ErrorPriceNotFound)
	}
	gamePrices := container.Prices[0]
	lowestPrice := gamePrices.RegularPrice.RawValue

	if gamePrices.DiscountPrice.RawValue != "" {
		lowestPrice = gamePrices.DiscountPrice.RawValue
	}

	price, err := strconv.ParseFloat(lowestPrice, 64)
	if err != nil {
		return 0, fmt.Errorf("getAPIPriceFromNsuID %w", err)
	}
	return price, nil
}

func getNsuID(pageData []byte) (int64, error) {
	nsuIDmatch := reNsuID.FindSubmatch(pageData)
	if nsuIDmatch == nil {
		return 0, ErrorNsuIDNotFound
	}

	nsuID, err := strconv.ParseInt(string(nsuIDmatch[1]), 10, 64)
	if err != nil {
		return 0, fmt.Errorf("getAPIPriceFromPage: %w", err)
	}

	return nsuID, nil
}

func getPriceAPIData(nsuID int64, country string, lang string) (*APIPriceContainer, error) {
	apiRoute := fmt.Sprintf(`https://api.ec.nintendo.com/v1/price?country=%v&lang=%v&ids=%v`, country, lang, nsuID)
	rawData, err := getURLData(apiRoute)
	if err != nil {
		return nil, fmt.Errorf("getPriceAPIData: %w", err)
	}

	var container APIPriceContainer
	err = json.Unmarshal(rawData, &container)
	if err != nil {
		return nil, fmt.Errorf("getPriceAPIData: %w", err)
	}

	return &container, nil
}

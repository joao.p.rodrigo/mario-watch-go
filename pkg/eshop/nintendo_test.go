package eshop_test

import (
	"testing"

	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/eshop"
)

var validURL = "https://www.nintendo.pt/Jogos/Nintendo-Switch/Pokemon-Shining-Pearl-1930567.html"

var validGame *eshop.WebsiteGame

func game(t *testing.T) *eshop.WebsiteGame {
	if validGame != nil {
		return validGame
	}
	var err error
	validGame, err = eshop.New(validURL)
	if err != nil {
		t.Fatal("New(): Can't make a new Pokemon Shining Pearl Game")
	}
	return validGame
}

func TestNewValidURL(t *testing.T) {
	game(t)
}

func TestNewInvalidURL(t *testing.T) {
	_, err := eshop.New("")
	if err == nil {
		t.Error("New(): Didn't throw when passing empty string")
	}
}

func TestNewInvalidRegion(t *testing.T) {
	_, err := eshop.New("https://www.nintendo.com/games/detail/the-legend-of-zelda-skyward-sword-hd-switch/")
	if err == nil {
		t.Error("New(): Didn't throw when US Region")
	}
}

func TestName(t *testing.T) {
	g := game(t)

	n := g.Name
	if n != "Pokémon Shining Pearl" {
		t.Errorf("Name(): No matched, result was '%v'", n)
	}
}

func TestPrice(t *testing.T) {
	g := game(t)

	price, currency := g.Price, g.Currency
	if currency != "€" {
		t.Errorf("Price(): expected €, result was '%v'", currency)
	}

	if price < 5 {
		t.Errorf("Price(): price too low to be true, result was '%v'", price)
	}
}

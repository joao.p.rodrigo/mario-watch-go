package telegram

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/mario"
	"log"
	"os"
	"reflect"
	"testing"
	"time"
)

type mockAPI struct {
	throwOnSend                bool
	throwOnGetUpdatesChan      bool
	stopReceivingUpdatesCalled bool
	sendCalled                 bool
	channel                    tgbotapi.UpdatesChannel
}

func (m *mockAPI) GetUpdatesChan(_ tgbotapi.UpdateConfig) (tgbotapi.UpdatesChannel, error) {
	if m.throwOnGetUpdatesChan {
		return nil, errors.New("expect Mock Error")
	}
	ch := make(tgbotapi.UpdatesChannel)
	m.channel = ch
	return ch, nil
}

func (m *mockAPI) StopReceivingUpdates() {
	m.stopReceivingUpdatesCalled = true
}

func (m *mockAPI) Send(_ tgbotapi.Chattable) (tgbotapi.Message, error) {
	m.sendCalled = true
	if m.throwOnSend {
		return tgbotapi.Message{}, errors.New("expect Mock Error")
	}
	return tgbotapi.Message{}, nil
}

func TestBot_Notify(t *testing.T) {
	type args struct {
		userID  string
		message string
	}
	tests := []struct {
		name    string
		api     botAPI
		args    args
		wantErr bool
	}{
		{name: "success", api: &mockAPI{throwOnSend: false}, args: args{userID: "123#", message: "oi"}, wantErr: false},
		{name: "fail on user", api: &mockAPI{throwOnSend: false}, args: args{userID: "123", message: "oi"}, wantErr: true},
		{name: "fail on send", api: &mockAPI{throwOnSend: true}, args: args{userID: "123#", message: "oi"}, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &Bot{
				botAPI: tt.api,
			}
			if err := b.Notify(tt.args.userID, tt.args.message); (err != nil) != tt.wantErr {
				t.Errorf("Notify() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestBot_Start(t *testing.T) {
	api := mockAPI{
		throwOnGetUpdatesChan: true,
	}
	b := Bot{
		BotConfig: BotConfig{
			Log: log.Default(),
		},
		botAPI: &api,
	}
	ctx, cancel := context.WithCancel(context.Background())

	err := b.Start(ctx)
	if err == nil {
		t.Errorf("Start: should have thrown error but didn't")
	}

	api.throwOnGetUpdatesChan = false
	go func() {
		err := b.Start(ctx)
		if err != nil {
			t.Errorf("Start: unexpected error %v", err)
		}
	}()
	time.Sleep(time.Second / 10)
	if api.channel == nil {
		t.Errorf("Start: should have started a channel but didn't")
	}

	cancel()
	time.Sleep(time.Second / 10)
	if api.stopReceivingUpdatesCalled == false {
		t.Errorf("Start: should have called stop receiving updates by now but didn't")
	}
}

func TestBot_handleAddGame(t *testing.T) {
	type addFunc func(string, string) error
	type args struct {
		user      User
		arguments string
	}
	user := User{}
	logger := bytes.Buffer{}
	tests := []struct {
		name         string
		testFunction addFunc
		args         args
		throwError   bool
	}{
		{"no func", nil, args{user, ""}, true},
		{"normal", func(a, b string) error { return nil }, args{user, ""}, false},
		{"fail", func(a, b string) error { return errors.New("expect Error") }, args{user, ""}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &Bot{
				BotConfig: BotConfig{Log: log.New(&logger, "", 0)},
				botAPI:    &mockAPI{},
			}
			b.UserGamesAdd = tt.testFunction
			b.handleAddGame(tt.args.user, tt.args.arguments)
			if (len(logger.String()) > 0) != tt.throwError {
				t.Errorf("handleAddGame (%v) Expected error = %v, got the oposite", tt.name, tt.throwError)
			}
			logger.Reset()
		})
	}
}

func TestBot_handleDefault(t *testing.T) {
	logger := bytes.Buffer{}

	ms := mockAPI{throwOnSend: false}
	bot := &Bot{
		botAPI:    &ms,
		BotConfig: BotConfig{Log: log.New(&logger, "", 0)},
	}

	bot.handleDefault(User{
		UserName: "test",
		ChatID:   1,
	})
	if !ms.sendCalled {
		t.Error("handleDefault: Send was not called on the API when it should have")
	}
}

func TestBot_handleHelp(t *testing.T) {
	logger := bytes.Buffer{}

	ms := mockAPI{throwOnSend: false}
	bot := &Bot{
		botAPI:    &ms,
		BotConfig: BotConfig{Log: log.New(&logger, "", 0)},
	}

	bot.handleHelp(User{
		UserName: "test",
		ChatID:   1,
	})
	if !ms.sendCalled {
		t.Error("handleDefault: Send was not called on the API when it should have")
	}
}

func TestBot_handleIncomingUpdate(t *testing.T) {
	logger := bytes.Buffer{}
	ms := mockAPI{}
	bot := &Bot{
		botAPI:    &ms,
		BotConfig: BotConfig{Log: log.New(&logger, "", 0)},
	}
	bot.GameGet = func(a string) (*mario.Game, error) {
		return &mario.Game{
			Name: "test", URL: "test", NsuID: 0, Region: "PT", Language: "pt",
			Currency: "€", Price: 0, MaxPrice: 0, MinPrice: 0, DateAdded: time.Time{},
			DateUpdated: time.Time{}, PricePoints: nil,
		}, nil
	}
	bot.UserGamesAdd = func(a, b string) error { return nil }
	bot.UserGamesRemove = func(a, b string) error { return nil }
	tests := []struct {
		name           string
		updateDataPath string
		wantError      bool
	}{
		{"help", "./testdata/message_help.json", false},
		{"misstype", "./testdata/message_holp.json", false},
		{"no command", "./testdata/message_no_command.json", false},
		{"addgame", "./testdata/message_addgame.json", false},
		{"removegame", "./testdata/message_removegame.json", false},
		{"pollgame", "./testdata/message_game.json", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dat, err := os.ReadFile(tt.updateDataPath)
			if err != nil {
				t.Errorf("handleIncomingUpdate: Missing fixture: %v", tt.updateDataPath)
				return
			}
			var update tgbotapi.Update
			json.Unmarshal(dat, &update)

			bot.handleIncomingUpdate(update)
			if (len(logger.String()) > 0) != tt.wantError {
				t.Errorf("handleIncomingUpdate (%v): Threw error but shouldn't", tt.name)
			}
			if !ms.sendCalled {
				t.Errorf("handleIncomingUpdate (%v): Didn't send message but should have", tt.name)
			}

		})
	}
}

func TestBot_handlePollGame(t *testing.T) {
	type getFunc func(string) (*mario.Game, error)
	g := mario.Game{
		Name:        "test",
		URL:         "test",
		NsuID:       0,
		Region:      "PT",
		Language:    "pt",
		Currency:    "€",
		Price:       0,
		MaxPrice:    0,
		MinPrice:    0,
		DateAdded:   time.Time{},
		DateUpdated: time.Time{},
		PricePoints: nil,
	}
	type args struct {
		user      User
		arguments string
	}
	user := User{}
	logger := bytes.Buffer{}
	tests := []struct {
		name         string
		testFunction getFunc
		args         args
		throwError   bool
	}{
		{"no func", nil, args{user, ""}, true},
		{"normal", func(a string) (*mario.Game, error) { return &g, nil }, args{user, ""}, false},
		{"fail", func(a string) (*mario.Game, error) { return nil, errors.New("expect Error") }, args{user, ""}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &Bot{
				BotConfig: BotConfig{Log: log.New(&logger, "", 0)},
				botAPI:    &mockAPI{},
			}
			b.GameGet = tt.testFunction
			b.handlePollGame(tt.args.user, tt.args.arguments)
			if (len(logger.String()) > 0) != tt.throwError {
				t.Errorf("handlePollGame (%v) Expected error = %v, got the oposite", tt.name, tt.throwError)
			}
			logger.Reset()
		})
	}
}

func TestBot_handleRemoveGame(t *testing.T) {
	type removeFunc func(string, string) error
	type args struct {
		user      User
		arguments string
	}
	user := User{}
	logger := bytes.Buffer{}
	tests := []struct {
		name         string
		testFunction removeFunc
		args         args
		throwError   bool
	}{
		{"no func", nil, args{user, ""}, true},
		{"normal", func(a, b string) error { return nil }, args{user, ""}, false},
		{"fail", func(a, b string) error { return errors.New("expect Error") }, args{user, ""}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &Bot{
				BotConfig: BotConfig{Log: log.New(&logger, "", 0)},
				botAPI:    &mockAPI{},
			}
			b.UserGamesRemove = tt.testFunction
			b.handleRemoveGame(tt.args.user, tt.args.arguments)
			if (len(logger.String()) > 0) != tt.throwError {
				t.Errorf("handleRemoveGame (%v) Expected error = %v, got the oposite", tt.name, tt.throwError)
			}
			logger.Reset()
		})
	}
}

func TestBot_logSend(t *testing.T) {
	logger := bytes.Buffer{}

	ms := mockAPI{throwOnSend: false}
	bot := &Bot{
		botAPI:    &ms,
		BotConfig: BotConfig{Log: log.New(&logger, "", 0)},
	}

	bot.logSend(1, "hi")
	if !ms.sendCalled {
		t.Error("logSend: Send was not called on the API when it should have")
	}

	ms.throwOnSend = true
	bot.logSend(1, "hi")
	got := logger.String()
	if len(got) == 0 {
		t.Error("logSend: When the API failed, nothing was written to the log")
	}

}

func TestNew(t *testing.T) {
	type args struct {
		botConfig BotConfig
	}
	tests := []struct {
		name    string
		args    args
		want    *Bot
		wantErr bool
	}{
		// New can only be tested with a real token as it invariably turns into integration testing.
		// So that is not implemented and coverage will suffer.
		{"no Log", args{}, nil, true},
		{"no token", args{botConfig: BotConfig{Token: "", Log: log.Default()}}, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := New(tt.args.botConfig)
			if (err != nil) != tt.wantErr {
				t.Errorf("New() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUser_FromString(t *testing.T) {
	type fields struct {
		UserName string
		ChatID   int64
	}
	type args struct {
		userString string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		result  fields
	}{
		{"normal", args{userString: "64#test"}, false, fields{UserName: "test", ChatID: 100}},
		{"wrong chatID", args{userString: "notanumber#test"}, true, fields{UserName: "test", ChatID: 100}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &User{}
			if err := u.FromString(tt.args.userString); (err != nil) != tt.wantErr {
				t.Errorf("FromString() error = %v, wantErr %v", err, tt.wantErr)
			}
			if (!tt.wantErr) && (tt.result.UserName != u.UserName) {
				t.Errorf("FromString() expected user %v, got %v", tt.result.UserName, u.UserName)
			}
			if (!tt.wantErr) && (tt.result.ChatID != u.ChatID) {
				t.Errorf("FromString() expected ChatID %v, got %v", tt.result.ChatID, u.ChatID)
			}
		})
	}
}

func TestUser_ToString(t *testing.T) {
	type fields struct {
		UserName string
		ChatID   int64
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{name: "normal", fields: fields{UserName: "test", ChatID: 100}, want: "64#test"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &User{
				UserName: tt.fields.UserName,
				ChatID:   tt.fields.ChatID,
			}
			if got := u.ToString(); got != tt.want {
				t.Errorf("ToString() = %v, want %v", got, tt.want)
			}
		})
	}
}

package telegram

import (
	"context"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/mario"
	"log"
	"strconv"
	"strings"
)

// Bot allows telegram communication, meaning receiving and sending messages to/from a user.
//
// It implements the mario.NotificationService interface for user notifications.
type Bot struct {
	BotConfig

	botAPI botAPI
}

// BotConfig is the configuration of the Bot.
//
// It takes a Token that must be gotten from the telegram botfather (see README), a logger and functions that connect it
// to the mario.Service that has been instantiated in main, usually.
//
// For full functionality, all struct parameters must have a valid value, though
// not setting the service functions will fail gracefully, notifying the chat user of a generic failure and
// logging the problem.
type BotConfig struct {
	Token           string
	Log             *log.Logger
	GameGet         func(url string) (*mario.Game, error)
	UserGamesAdd    func(userID string, gameURL string) error
	UserGamesRemove func(userID string, gameURL string) error
	Debug           bool
}

type botAPI interface {
	GetUpdatesChan(c tgbotapi.UpdateConfig) (tgbotapi.UpdatesChannel, error)
	StopReceivingUpdates()
	Send(c tgbotapi.Chattable) (tgbotapi.Message, error)
}

// User is the representation of a user in Telegram.
//
// Telegram communication is not made using a userID, instead it uses a chat ID.
// Since we need to send messages to the user that are not immediate responses, there is a need to persist this chatID.
// Therefore, User keeps the UserName (for eventual message customization, not used at the moment) and ChatID and has
// functions for converting between the struct and a string in the form "<ChatID Hex>#<UserName>"
type User struct {
	UserName string
	ChatID   int64
}

// ToString converts the User to the format "<ChatID Hex>#<UserName>"
func (u *User) ToString() string {
	return strconv.FormatInt(u.ChatID, 16) + "#" + u.UserName
}

// FromString receives a string in the form "<ChatID Hex>#<UserName>" and fills the User struct with it's details.
func (u *User) FromString(userString string) error {
	userSlice := strings.Split(userString, "#")
	if len(userSlice) != 2 {
		return fmt.Errorf("User.FromString (telegram): user %v doesnt conform to <chatIDHex>#<username>", userString)
	}

	chatID, err := strconv.ParseInt(userSlice[0], 16, 64)
	if err != nil {
		return fmt.Errorf("User.FromString (telegram): %w", err)
	}

	u.ChatID = chatID
	u.UserName = userSlice[1]

	return nil
}

// New allows for safe instantiation of the Bot, checking for the botConfig validity and creating a botAPI with the
// provided Token.
func New(botConfig BotConfig) (*Bot, error) {
	if botConfig.Log == nil {
		return nil, fmt.Errorf("bot.New: log parameter must be set")
	}
	bot := Bot{BotConfig: botConfig}

	botAPI, err := tgbotapi.NewBotAPI(bot.Token)
	if err != nil {
		return nil, fmt.Errorf("new (Bot): %w", err)
	}
	botAPI.Debug = botConfig.Debug

	bot.botAPI = botAPI

	return &bot, nil
}

// Start listens for the botAPI incoming messages, and handles them. It also listens for the provided context, so it
// can stop when a signal is sent.
func (b *Bot) Start(ctx context.Context) error {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := b.botAPI.GetUpdatesChan(u)
	if err != nil {
		return fmt.Errorf("start (Bot): %w", err)
	}

	b.Log.Println("Telegram Bot started listening")

	for {
		select {
		case <-ctx.Done():
			b.botAPI.StopReceivingUpdates()
			b.Log.Println("Telegram Bot stopped listening")
			return nil
		case update := <-updates:
			go b.handleIncomingUpdate(update)
		}
	}
}

// handleIncomingUpdate is the root handler of a telegram message. It routes it to the specific handlers of each allowed
// command, or the default handler if the message can't be handled.
func (b *Bot) handleIncomingUpdate(update tgbotapi.Update) {
	user := User{
		ChatID:   update.Message.Chat.ID,
		UserName: update.Message.From.UserName,
	}

	if update.Message == nil { // ignore any non-Message updates
		return
	}

	if !update.Message.IsCommand() { // ignore any non-command Messages
		b.handleDefault(user)
		return
	}

	switch update.Message.Command() {
	case "help":
		b.handleHelp(user)
	case "addgame":
		b.handleAddGame(user, update.Message.CommandArguments())
	case "removegame":
		b.handleRemoveGame(user, update.Message.CommandArguments())
	case "game":
		b.handlePollGame(user, update.Message.CommandArguments())
	default:
		b.handleDefault(user)
	}
}

// Notify sends a user of the given userID a message.
func (b *Bot) Notify(userID string, message string) error {
	var user User
	if err := user.FromString(userID); err != nil {
		return fmt.Errorf("bot.Notify (telegram): %w", err)
	}

	msg := tgbotapi.NewMessage(user.ChatID, message)
	_, err := b.botAPI.Send(msg)
	if err != nil {
		return fmt.Errorf("bot.Notify (telegram): %w", err)
	}

	return nil
}

// logSend wraps the botAPI.Send function, and logs errors that may arise when sending a message to a user.
func (b *Bot) logSend(chatID int64, text string) {
	msg := tgbotapi.NewMessage(chatID, text)
	_, err := b.botAPI.Send(msg)
	if err != nil {
		b.Log.Printf("ERR: Send Message %v", err)
	}
}

// handleHelp returns the user a help message.
func (b *Bot) handleHelp(user User) {
	msg := `Options:
		/help: Display this help message
		/game <url> Get details of a game such as price, minimum and maximum recorded prices
		/addgame <url> Subscribe to the game's price drops
		/removegame <url> Unsubscribe to the game's price drops

		<url> should be an eShop link to the game.
`
	b.logSend(user.ChatID, msg)
}

// handleDefault returns the user a default response when commands couldn't be interpreted.
func (b *Bot) handleDefault(user User) {
	b.logSend(user.ChatID, "I don't know that command. Type /help for all the options")
}

// handlePollGame handles requests from a user to know about a specific game without subscribing to it.
//
// arguments must be a URL of the game in the nintendo eshop.
func (b *Bot) handlePollGame(user User, arguments string) {
	if b.GameGet == nil {
		b.Log.Println("ERR GameGet Function not Set")
		b.logSend(user.ChatID, "An Error Occurred")
		return
	}
	g, err := b.GameGet(arguments)
	if err != nil {
		b.Log.Printf("ERR GameGet %v", err)
		b.logSend(user.ChatID, "An Error Occurred")
		return
	}

	msg := g.String()
	b.logSend(user.ChatID, msg)
}

// handleAddGame handles requests from a user to subscribe to a game.
//
// arguments must be a URL of the game in the nintendo eshop.
func (b *Bot) handleAddGame(user User, arguments string) {
	if b.UserGamesAdd == nil {
		b.Log.Println("ERR UserGamesAdd Function not set")
		b.logSend(user.ChatID, "An Error Occurred")
		return
	}

	err := b.UserGamesAdd(user.ToString(), arguments)
	if err != nil {
		b.Log.Printf("Err UserGamesAdd %v", err)
		b.logSend(user.ChatID, "Your operation did not complete, sorry...")
		return
	}

	msg := "You have subscribed to the game and will receive notifications once it's cheaper!"
	b.logSend(user.ChatID, msg)
}

// handleRemoveGame handles requests for a user to unsubscribe to a game.
//
// arguments must be a URL of the game in the nintendo eshop.
func (b *Bot) handleRemoveGame(user User, arguments string) {
	if b.UserGamesRemove == nil {
		b.Log.Println("ERR UserGamesRemove Function not set")
		b.logSend(user.ChatID, "An Error Occurred")
		return
	}

	err := b.UserGamesRemove(user.ToString(), arguments)
	if err != nil {
		b.Log.Printf("Err UserGamesRemove %v", err)
		b.logSend(user.ChatID, "Your operation did not complete, sorry...")
		return
	}

	msg := "You have removed your subscription from this game and will no longer get updates. You can always subscribe again."
	b.logSend(user.ChatID, msg)
}

package mongo

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/mario"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type mongoGame struct {
	Name            string             `bson:"name"`
	URL             string             `bson:"url"`
	NsuID           int64              `bson:"nsuid"`
	Region          string             `bson:"region"`
	Language        string             `bson:"language"`
	Currency        string             `bson:"currency"`
	Price           float64            `bson:"price"`
	MaxPrice        float64            `bson:"max_price"`
	MinPrice        float64            `bson:"min_price"`
	DateAdded       primitive.DateTime `bson:"date_added"`
	DateUpdated     primitive.DateTime `bson:"date_updated"`
	SubscribedUsers []string           `bson:"subscribed_users"`
}

func (m *mongoGame) fromGame(g mario.Game) {
	*m = mongoGame{
		Name:            g.Name,
		URL:             g.URL,
		NsuID:           g.NsuID,
		Region:          g.Region,
		Language:        g.Language,
		Currency:        g.Currency,
		Price:           g.Price,
		MaxPrice:        g.MaxPrice,
		MinPrice:        g.MinPrice,
		DateAdded:       primitive.NewDateTimeFromTime(g.DateAdded),
		DateUpdated:     primitive.NewDateTimeFromTime(g.DateUpdated),
		SubscribedUsers: []string{},
	}
}

func (m *mongoGame) toGame() mario.Game {
	return mario.Game{
		Name:        m.Name,
		URL:         m.URL,
		NsuID:       m.NsuID,
		Region:      m.Region,
		Language:    m.Language,
		Currency:    m.Currency,
		Price:       m.Price,
		MaxPrice:    m.MaxPrice,
		MinPrice:    m.MinPrice,
		DateAdded:   m.DateAdded.Time(),
		DateUpdated: m.DateUpdated.Time(),
	}
}

func mongoGamesToGames(g []mongoGame) []mario.Game {
	games := make([]mario.Game, len(g))
	for i, m := range g {
		games[i] = m.toGame()
	}
	return games
}

func (db *DB) GamesGet(limit int, offset int) ([]mario.Game, error) {
	skipOptions := options.Find().SetSort(bson.D{{"nsuid", 1}}).SetSkip(int64(offset))
	limitOptions := options.Find().SetLimit(int64(limit))
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	var games []mongoGame
	cursor, err := db.gamesCollection.Find(ctx, bson.D{}, skipOptions, limitOptions)
	if err != nil {
		return nil, fmt.Errorf("GamesGet: %w", err)
	}
	err = cursor.Decode(games)
	if err != nil {
		return nil, fmt.Errorf("GamesGet: %w", err)
	}
	return mongoGamesToGames(games), nil
}

func (db *DB) GameGetByURL(url string) (*mario.Game, error) {
	game, err := db.mongoGameGetByURL(url)
	if err != nil {
		return nil, fmt.Errorf("GameGetByURL: %w", err)
	}
	g := game.toGame()
	return &g, nil
}

func (db *DB) mongoGameGetByURL(url string) (*mongoGame, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	var game mongoGame
	err := db.gamesCollection.FindOne(ctx, bson.D{{"url", url}}).Decode(&game)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, mario.ErrorGameNotFound
		}
		return nil, fmt.Errorf("mongoGameGetByURL %w", err)
	}

	return &game, nil
}

func (db *DB) GameInsert(game *mario.Game) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	now := time.Now()
	game.DateUpdated = now
	game.DateAdded = now
	mg := mongoGame{}
	mg.fromGame(*game)
	_, err := db.gamesCollection.InsertOne(ctx, mg)
	if err != nil {
		return fmt.Errorf("GameInsert %w", err)
	}
	return nil
}

func (db *DB) GameUpdate(url string, game *mario.Game) error {
	filter := bson.D{{"url", url}}
	mg := mongoGame{}
	mg.fromGame(*game)
	_, err := db.gamesCollection.ReplaceOne(context.TODO(), filter, mg)
	if err != nil {
		return fmt.Errorf("GameUpdate: %w", err)
	}
	return nil
}

func (db *DB) GamesGetUpdatedBefore(timestamp time.Time, limit int) ([]mario.Game, error) {
	limitOptions := options.Find().SetLimit(int64(limit))
	filter := bson.M{
		"date_updated": bson.M{"&lte": primitive.NewDateTimeFromTime(timestamp)},
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	var games []mongoGame
	cursor, err := db.gamesCollection.Find(ctx, filter, limitOptions)
	if err != nil {
		return nil, fmt.Errorf("GamesGet: %w", err)
	}
	err = cursor.Decode(games)
	if err != nil {
		return nil, fmt.Errorf("GamesGet: %w", err)
	}
	return mongoGamesToGames(games), nil
}

func (db *DB) GameAddPricePoint(url string, pp *mario.PricePoint) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := db.pricepointsCollection.InsertOne(ctx,
		bson.M{"gameURL": url, "timestamp": primitive.NewDateTimeFromTime(pp.Timestamp), "price": pp.Price},
	)
	if err != nil {
		return fmt.Errorf("GameAddPricePoint %w", err)
	}
	return nil
}

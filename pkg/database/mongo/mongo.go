package mongo

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"go.mongodb.org/mongo-driver/x/mongo/driver/connstring"
	"time"
)

type DB struct {
	client                *mongo.Client
	database              *mongo.Database
	gamesCollection       *mongo.Collection
	usersCollection       *mongo.Collection
	pricepointsCollection *mongo.Collection
}

func New(url string) (*DB, error) {
	cs, err := connstring.ParseAndValidate(url)
	if err != nil {
		return nil, fmt.Errorf("mongo.New %w", err)
	}
	clientOptions := options.Client().
		ApplyURI(url)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		return nil, fmt.Errorf("mongo.New %w", err)
	}
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return nil, fmt.Errorf("mongo.New %w", err)
	}
	db := client.Database(cs.Database)
	gamesCol := db.Collection("games")
	usersCol := db.Collection("users")
	pricepointsCol := db.Collection("price_points")

	return &DB{
		client:                client,
		database:              db,
		gamesCollection:       gamesCol,
		usersCollection:       usersCol,
		pricepointsCollection: pricepointsCol,
	}, nil
}

func BuildMongoURL(clusterURL string, database string, user string, password string) string {
	return fmt.Sprintf("mongodb+srv://%v:%v@%v/%v?retryWrites=true&w=majority",
		user, password, clusterURL, database)
}

package mongo

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/mario"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type mongoUser struct {
	ID          string             `bson:"telegram_id"`  // ID is the handle used to communicate with the user (usually on telegram)
	DateCreated primitive.DateTime `bson:"date_created"` // DateCreated is the date of first user interaction that required storage.
}

func (u *mongoUser) toUser() mario.User {
	return mario.User{
		ID:          u.ID,
		DateCreated: u.DateCreated.Time(),
	}
}

func (u *mongoUser) fromUser(m *mario.User) {
	*u = mongoUser{
		ID:          m.ID,
		DateCreated: primitive.NewDateTimeFromTime(m.DateCreated),
	}
}

func mongoUsersToUsers(mu []mongoUser) []mario.User {
	users := make([]mario.User, len(mu))
	for i, u := range mu {
		users[i] = u.toUser()
	}
	return users
}

func (db *DB) UsersGetByGameURL(url string) ([]mario.User, error) {
	game, err := db.mongoGameGetByURL(url)
	if err != nil {
		return nil, fmt.Errorf("UsersGetByGameURL: %w", err)
	}

	// Since this function is used only by notifications, we can get away with only adding the user telegram handle
	// Monsters beware, though
	users := make([]mario.User, len(game.SubscribedUsers))
	for i, u := range game.SubscribedUsers {
		users[i] = mario.User{ID: u}
	}

	return users, nil
}

func (db *DB) UserAdd(user mario.User) error {
	var mu mongoUser

	user.DateCreated = time.Now()
	mu.fromUser(&user)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := db.usersCollection.InsertOne(ctx, mu)
	if err != nil {
		return fmt.Errorf("UserAdd: %w", err)
	}
	return nil
}

func (db *DB) UserGet(userID string) (*mario.User, error) {
	var mu mongoUser

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	err := db.usersCollection.FindOne(ctx, bson.M{"telegram_id": userID}).Decode(&mu)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, mario.ErrorUserNotFound
		}
		return nil, fmt.Errorf("UserGet: %w", err)
	}

	u := mu.toUser()
	return &u, nil
}

func (db *DB) UserGamesAdd(userID string, gameURL string) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := db.gamesCollection.UpdateOne(ctx, bson.M{"url": gameURL}, bson.D{
		{"$addToSet", bson.M{"subscribed_users": userID}},
	})
	if err != nil {
		return fmt.Errorf("UserGamesAdd: %w", err)
	}
	return nil
}

func (db *DB) UserGamesRemove(userID string, gameURL string) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := db.gamesCollection.UpdateOne(ctx, bson.M{"url": gameURL}, bson.M{"$pull": bson.M{"subscribed_users": userID}})
	if err != nil {
		return fmt.Errorf("UserGamesRemove: %w", err)
	}
	return nil
}

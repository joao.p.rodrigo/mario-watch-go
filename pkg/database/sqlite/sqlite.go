package sqlite

import (
	"embed"
	"errors"
	"net/http"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/sqlite3"
	"github.com/golang-migrate/migrate/v4/source/httpfs"
	"github.com/jmoiron/sqlx"
)

//go:embed migrations
var migrationsFS embed.FS

type DB struct {
	*sqlx.DB
}

func NewSQLite(dbURL string) (*DB, error) {
	db, err := sqlx.Connect("sqlite3", dbURL)
	if err != nil {
		return nil, err
	}
	return &DB{DB: db}, nil
}

func (s *DB) UpdateDB() error {
	source, err := httpfs.New(http.FS(migrationsFS), "migrations")
	if err != nil {
		return err
	}
	driver, err := sqlite3.WithInstance(s.DB.DB, &sqlite3.Config{})
	if err != nil {
		return err
	}
	m, err := migrate.NewWithInstance("maindb-source", source, "maindb-driver", driver)
	if err != nil {
		return err
	}
	err = m.Up()
	if (err != nil) && (!errors.Is(err, migrate.ErrNoChange)) {
		return err
	}
	return nil
}

package sqlite

import (
	"database/sql"
	"errors"
	"fmt"
	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/mario"
	"time"
)

func (s *DB) UsersGetByGameURL(url string) ([]mario.User, error) {
	stmt := `SELECT DISTINCT u.user_id, u.date_created 
		FROM user u
		JOIN user_game ug on u.id = ug.user_id
    	JOIN game g on ug.game_id = g.id
    	WHERE g.url = ?`
	var users []mario.User
	err := s.Select(&users, stmt, url)
	if err != nil {
		return nil, fmt.Errorf("UsersGetByGameURL: %w", err)
	}

	return users, nil
}

func (s *DB) UserAdd(user mario.User) error {
	stmt := `INSERT INTO user (user_id, date_created) VALUES (:user_id, :date_created)`
	user.DateCreated = time.Now()
	_, err := s.NamedExec(stmt, user)
	if err != nil {
		return fmt.Errorf("UserAdd: %w", err)
	}

	return nil
}

func (s DB) UserGet(userID string) (*mario.User, error) {
	stmt := `SELECT user_id, date_created FROM user WHERE user_id = ?`
	var user mario.User

	err := s.Get(&user, stmt, userID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, mario.ErrorUserNotFound
		}
		return nil, fmt.Errorf("UserGet: %w", err)
	}
	return &user, nil
}

func (s DB) UserGamesAdd(userID string, gameURL string) error {
	stmt := `INSERT INTO user_game (user_id, game_id) VALUES (
	(SELECT id FROM user WHERE user.user_id = :user_id),
	(SELECT id FROM game WHERE game.url = :game_url)
)`
	_, err := s.NamedExec(stmt, map[string]interface{}{"user_id": userID, "game_url": gameURL})
	if err != nil {
		return fmt.Errorf("UserGamesAdd: %w", err)
	}

	return nil
}

func (s DB) UserGamesRemove(userID string, gameURL string) error {
	stmt := `DELETE FROM user_game 
				WHERE user_id = (SELECT id FROM user WHERE user.user_id = :user_id) 
				AND game_id = (SELECT id FROM game WHERE game.url = :game_url)`
	_, err := s.NamedExec(stmt, map[string]interface{}{"user_id": userID, "game_url": gameURL})
	if err != nil {
		return fmt.Errorf("UserGamesRemove: %w", err)
	}

	return nil
}

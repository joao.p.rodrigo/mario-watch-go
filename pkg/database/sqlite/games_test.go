package sqlite

import (
	"testing"

	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/mario"
)

var db *DB

var mockGame = mario.Game{URL: "testURL.com", Name: "testInsert", Region: "PT", Currency: "€", Price: 55.0}
var mockUpdatedGame = mario.Game{URL: "testupdate.com", Name: "testUpdate", Region: "ES", Currency: "€", Price: 50.0}

func getDB() *DB {
	if db == nil {
		url := ":memory:"
		startupDB, err := NewSQLite(url)
		if err != nil {
			panic(err)
		}
		err = startupDB.UpdateDB()
		if err != nil {
			panic(err)
		}
		db = startupDB
	}

	return db
}

func TestInsert(t *testing.T) {
	tempDB := getDB()

	err := tempDB.GameInsert(&mockGame)
	if err != nil {
		t.Errorf("GameInsert: %v", err)
	}
}

func TestFailRepeatInsert(t *testing.T) {
	tempDB := getDB()

	err := tempDB.GameInsert(&mockGame)
	if err == nil {
		t.Error("GameInsert Fail on Repeat: did not fail")
	}
}

func TestGameGetByURL(t *testing.T) {
	tempDB := getDB()

	url := mockGame.URL

	g, err := tempDB.GameGetByURL(url)
	if err != nil {
		t.Errorf("GameGet %v: %v", url, err)
	}

	if g.Name != mockGame.Name || g.URL != mockGame.URL || g.Region != mockGame.Region || g.Currency != mockGame.Currency {
		t.Errorf("GameGet %v: database result did not match", url)
	}
}

func TestUpdate(t *testing.T) {
	tempDB := getDB()
	url := mockGame.URL

	err := tempDB.GameUpdate(url, &mockUpdatedGame)
	if err != nil {
		t.Errorf("GameUpdate %v: %v", url, err)
	}

}

func TestGetGames(t *testing.T) {
	tempDB := getDB()

	games, err := tempDB.GamesGet(100, 0)
	if err != nil {
		t.Errorf("GamesGet: %v", err)
	}
	game := games[0]
	if game.Name != mockUpdatedGame.Name || game.URL != mockUpdatedGame.URL || game.Region != mockUpdatedGame.Region || game.Currency != mockUpdatedGame.Currency {
		t.Errorf("GamesGet: updatedGame does not match %v != %v", &mockUpdatedGame, games[0])
	}

}

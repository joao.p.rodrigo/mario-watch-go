package sqlite

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/mario"
)

func (s *DB) GamesGet(limit int, offset int) ([]mario.Game, error) {
	stmt := `SELECT 
		url, nsuid, name, region, language, price, currency, max_price, min_price, date_added, date_updated
		FROM GAME ORDER BY id LIMIT ? OFFSET ?`
	var games []mario.Game
	err := s.Select(&games, stmt, limit, offset)
	if err != nil {
		return nil, fmt.Errorf("GamesGet: %w", err)
	}

	return games, nil
}

func (s *DB) GamesGetUpdatedBefore(timestamp time.Time, limit int) ([]mario.Game, error) {
	stmt := `SELECT 
			url, nsuid, name, region, language, price, currency, max_price, min_price, date_added, date_updated
			FROM GAME WHERE date_updated < ? LIMIT ?`
	var games []mario.Game
	err := s.Select(&games, stmt, timestamp, limit)
	if err != nil {
		return nil, fmt.Errorf("GamesGet: %w", err)
	}

	return games, nil
}

func (s *DB) GameGetByURL(url string) (*mario.Game, error) {
	stmt := `SELECT
			url, nsuID, name, region, language, price, currency, max_price, min_price, date_added, date_updated
			FROM GAME WHERE url = ?`
	game := &mario.Game{}

	err := s.Get(game, stmt, url)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, mario.ErrorGameNotFound
		}
		return nil, fmt.Errorf("GameGetByURL: %w", err)
	}
	return game, nil
}

func (s *DB) GameInsert(game *mario.Game) error {
	stmt := `INSERT INTO GAME
    (url, nsuid, name, region, language, price, currency, max_price, min_price, date_added, date_updated)
    VALUES
	(:url, :nsuid, :name, :region, :language, :price, :currency, :max_price, :min_price, DATETIME('now'), DATETIME('now'))
	`
	game.DateAdded = time.Now()
	game.DateUpdated = time.Now()
	_, err := s.NamedExec(stmt, game)
	if err != nil {
		return fmt.Errorf("GameInsert: %w", err)
	}

	return nil
}

func (s *DB) GameUpdate(url string, game *mario.Game) error {
	stmt := `UPDATE GAME SET
        		 url = :url,
                 nsuid = :nsuid, 
                 name = :name,
                 region = :region,
                 language = :language,
                 price = :price,
                 currency = :currency,
                 max_price = :maxPrice,
                 min_price = :minPrice,
                 date_updated = DATETIME('now')
		WHERE url = :original_url
	`
	values := map[string]interface{}{
		"original_url": url,
		"url":          game.URL,
		"nsuid":        game.NsuID,
		"name":         game.Name,
		"region":       game.Region,
		"language":     game.Language,
		"price":        game.Price,
		"currency":     game.Currency,
		"maxPrice":     game.MaxPrice,
		"minPrice":     game.MinPrice,
	}

	_, err := s.NamedExec(stmt, values)
	if err != nil {
		return fmt.Errorf("GameUpdate: %w", err)
	}
	return nil
}

func (s *DB) GameAddPricePoint(url string, pp *mario.PricePoint) error {
	stmt := `INSERT INTO price_point (game_id, date_created, price) VALUES 
            ((SELECT id FROM game WHERE url = :url), DATETIME('now'), :price)`
	_, err := s.NamedExec(stmt, map[string]interface{}{
		"url":         url,
		"dateCreated": pp.Timestamp,
		"price":       pp.Price,
	})
	if err != nil {
		return fmt.Errorf("GameAddPricePoint: %w", err)
	}
	return nil
}

func (s *DB) GameGetLatestPricePoints(url string, limit int, offset int) ([]mario.PricePoint, error) {
	stmt := `SELECT FROM price_point 
		JOIN game g on price_point.game_id = g.id
		WHERE g.url = ? ORDER BY date_created DESC LIMIT ? OFFSET ?`
	var pricePoints []mario.PricePoint

	err := s.Select(&pricePoints, stmt, url, limit, offset)
	if err != nil {
		return nil, fmt.Errorf("GameGetLatestPricePoints: %w", err)
	}
	return pricePoints, nil
}

CREATE TABLE IF NOT EXISTS user(
    id INTEGER PRIMARY KEY,
    user_id VARCHAR UNIQUE,
    date_created DATETIME
);

CREATE TABLE IF NOT EXISTS user_game(
                                    id INTEGER PRIMARY KEY,
                                    user_id INTEGER NOT NULL,
                                    game_id INTEGER NOT NULL,
                                    FOREIGN KEY(user_id) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE,
                                    FOREIGN KEY(game_id) REFERENCES game(id) ON DELETE CASCADE ON UPDATE CASCADE,
                                    UNIQUE(user_id, game_id)
);

package mario

import (
	"errors"
	"fmt"
	"time"
)

// User is the person that uses the watch service. This struct represents the data associated with the person.
type User struct {
	ID          string    `db:"user_id"`      // ID is the handle used to communicate with the user (usually on telegram)
	DateCreated time.Time `db:"date_created"` // DateCreated is the date of first user interaction that required storage.
}

// UserAdd creates and stores a new User
func (gc *Service) UserAdd(userID string) (*User, error) {
	user := User{
		ID:          userID,
		DateCreated: time.Now(),
	}
	err := gc.UserStore.UserAdd(user)
	if err != nil {
		return nil, fmt.Errorf("UserAdd: %w", err)
	}
	return &user, nil
}

// UserGetOrCreate returns a User from storage with given userID.
// If it does not exist, it asks to create it and then returns it.
func (gc *Service) UserGetOrCreate(userID string) (*User, error) {
	user, err := gc.UserStore.UserGet(userID)
	if err == nil {
		return user, nil
	}

	if !errors.Is(err, ErrorUserNotFound) {
		return nil, fmt.Errorf("service.UserGetOrCreate: %w", err)
	}

	user, err = gc.UserAdd(userID)
	if err != nil {
		return nil, fmt.Errorf("service.userGetOrCreate: %w", err)
	}

	return user, nil
}

// UserGamesAdd appends a game to the list of subscribed games of a user.
//
// It first checks if the user and game exist on storage. If they don't, they are created also.
func (gc *Service) UserGamesAdd(userID string, gameURL string) error {
	user, err := gc.UserGetOrCreate(userID)
	if err != nil {
		return fmt.Errorf("UserGamesAdd: %w", err)
	}

	game, err := gc.GameGetOrCreate(gameURL)
	if err != nil {
		return fmt.Errorf("UserGamesAdd: %w", err)
	}

	err = gc.UserStore.UserGamesAdd(user.ID, game.URL)
	if err != nil {
		return fmt.Errorf("UserGamesAdd: %w", err)
	}
	return nil
}

// UserGamesRemove removes a Game from the list of user's subscribed games.
func (gc *Service) UserGamesRemove(userID string, gameURL string) error {
	err := gc.UserStore.UserGamesRemove(userID, gameURL)
	if err != nil {
		return fmt.Errorf("UserGamesRemove: %w", err)
	}
	return nil
}

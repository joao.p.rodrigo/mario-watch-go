package mario

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

// Game is the Domain entity that represents a game's data
type Game struct {
	Name        string
	URL         string
	NsuID       int64 `db:"nsuid"`
	Region      string
	Language    string
	Currency    string
	Price       float64
	MaxPrice    float64   `db:"max_price"`
	MinPrice    float64   `db:"min_price"`
	DateAdded   time.Time `db:"date_added"`
	DateUpdated time.Time `db:"date_updated"`
	PricePoints []PricePoint
}

type gamePriceReduction struct {
	game     Game
	oldPrice float64
	newPrice float64
}

// WebsiteGame represents a game in the eshop store
type WebsiteGame struct {
	URL      string
	Language string
	Name     string
	Region   string
	Price    float64
	Currency string
}

// PricePoint is a price that a game had in a specific point in time
type PricePoint struct {
	Timestamp time.Time
	Price     float64
}

func (g *Game) String() string {
	msg := `Game %v,
URL: %v
Latest Price: %.2f%v`
	return fmt.Sprintf(msg, g.Name, g.URL, g.Price, g.Currency)
}

// GameCreate creates a Game, stores it and returns it.
//
// A new first Pricepoint is also stored along with the game data.
func (gc *Service) GameCreate(strippedGameURL string) (*Game, error) {
	g, err := gc.Shop.NewGame(strippedGameURL)
	if err != nil {
		return nil, fmt.Errorf("gameGetOrCreate: %w", err)
	}

	g.MaxPrice = g.Price
	g.MinPrice = g.Price

	err = gc.GameStore.GameInsert(g)
	if err != nil {
		return nil, fmt.Errorf("gameGetOrCreate: %w", err)
	}

	p := PricePoint{Price: g.Price, Timestamp: time.Now()}
	err = gc.GameStore.GameAddPricePoint(g.URL, &p)
	if err != nil {
		return nil, fmt.Errorf("gameGetOrCreate: %w", err)
	}
	return g, nil
}

// GameGetOrCreate returns a Game from the storage.
// If the game does not exist yet, it asks to create it, and returns it.
func (gc *Service) GameGetOrCreate(gameURL string) (*Game, error) {
	url := stripURL(gameURL)
	g, err := gc.GameStore.GameGetByURL(url)
	if err == nil {
		return g, nil
	}

	if !errors.Is(err, ErrorGameNotFound) {
		return nil, fmt.Errorf("gameGetOrCreate: %w", err)
	}

	g, err = gc.GameCreate(url)
	if err != nil {
		return nil, fmt.Errorf("gameGetOrCreate: %w", err)
	}
	return g, nil
}

func stripURL(url string) string {
	return strings.Split(url, "?")[0]
}

// WatchGames checks updated prices on a number (limit) of games.
// If the game has lowered in Price, users are notified.
func (gc *Service) WatchGames(limit int) {
	gc.Log.Printf("Checking price on max %v games", limit)
	cheaperGames, err := gc.updateGames(limit)
	if err != nil {
		gc.Log.Printf("WatchGames: %v", err)
	}

	for _, gamePriceData := range cheaperGames {
		gc.notifyPriceDrop(gamePriceData)
	}
}

// updateGames orchestrates the update of prices of a number of games, up to a defined limit.
//
// It returns the games that became cheaper
func (gc *Service) updateGames(limit int) ([]gamePriceReduction, error) {
	gamesToUpdate, err := gc.getOlderGames(limit)
	if err != nil {
		return nil, fmt.Errorf("updateGames: %w", err)
	}

	var cheaperGames []gamePriceReduction
	// TODO: transform into goroutines, add a way to timeout
	for _, g := range gamesToUpdate {
		oldPrice := g.Price
		cheaperPricepoint, err := gc.checkGamePriceFluctuation(&g)
		if err != nil {
			gc.Log.Print(err)
			continue
		}

		if cheaperPricepoint != nil {
			cheaperGames = append(cheaperGames, gamePriceReduction{g, oldPrice, cheaperPricepoint.Price})
		}
	}

	return cheaperGames, nil
}

// checkGamePriceFluctuation finds the current price in the shop.
// If the price changed, it's updated in the database;
// If the price is cheaper, it's also returned.
func (gc *Service) checkGamePriceFluctuation(game *Game) (*PricePoint, error) {
	pricepoint, err := gc.Shop.GetPriceFromGame(game)
	if err != nil {
		return nil, fmt.Errorf("CheckGamePriceFluctuation %v: %w", game.URL, err)
	}

	currentPrice := game.Price

	if currentPrice == pricepoint.Price {
		err = gc.GameStore.GameUpdate(game.URL, game)
		if err != nil {
			gc.Log.Printf("ERR checkGamePriceFluctuation: %v\n", err)
		}
		return nil, nil
	}

	gc.updateGamePrice(game, pricepoint)

	if pricepoint.Price < currentPrice {
		return pricepoint, nil
	}

	return nil, nil
}

// getOlderGames returns games with "stale" prices, classified as games updated more than 6 hours ago
func (gc *Service) getOlderGames(limit int) ([]Game, error) {
	limitDate := time.Now().Add(time.Hour * -6)
	games, err := gc.GameStore.GamesGetUpdatedBefore(limitDate, limit)
	if err != nil {
		return nil, fmt.Errorf("GetOlderGames: %w", err)
	}
	return games, nil
}

// updateGamePrice stores a new price in the database and refreshes the minimum and maximum prices.
func (gc *Service) updateGamePrice(game *Game, pricepoint *PricePoint) {
	err := gc.GameStore.GameAddPricePoint(game.URL, pricepoint)
	if err != nil {
		gc.Log.Printf("updateGamePrice: %v\n", err)
	}

	newPrice := pricepoint.Price
	game.Price = newPrice

	if newPrice > game.MaxPrice {
		game.MaxPrice = newPrice
	}

	if newPrice < game.MinPrice {
		game.MinPrice = newPrice
	}

	err = gc.GameStore.GameUpdate(game.URL, game)
	if err != nil {
		gc.Log.Printf("updateGamePrice: %v\n", err)
	}
}

// notifyPriceDrop finds the users subscribed to a game and notifies them that the price has gone down
func (gc *Service) notifyPriceDrop(data gamePriceReduction) {
	users, err := gc.UserStore.UsersGetByGameURL(data.game.URL)
	if err != nil {
		gc.Log.Printf("notifyPriceDrop %v: %v", data.game.URL, err)
	}

	priceReduction := data.newPrice - data.oldPrice
	priceReductionPct := priceReduction / data.oldPrice * 100

	message := fmt.Sprintf("Game %v is now cheaper by %.2f%v (%.2f%%)", data.game.Name, priceReduction, data.game.Currency, priceReductionPct)
	if data.newPrice == data.game.MinPrice {
		message += fmt.Sprintf("\n\nGame is at it's minimum price so far recorded!")
	}

	for _, user := range users {
		err := gc.Notifier.Notify(user.ID, message)
		if err != nil {
			gc.Log.Printf("notifyPriceDrop user %v: %v", user.ID, err)
		}
	}
}

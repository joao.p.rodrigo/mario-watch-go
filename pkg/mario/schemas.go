package mario

import (
	"errors"
	"log"
	"time"
)

// Service is the aggregator for the functions provided by the project.
//
// It's instantiation is the main service provider of the project.
// For full functionality, all struct parameters need to be filled.
type Service struct {
	GameStore GamesStore
	UserStore UserStore
	Notifier  NotificationService
	Shop      Website
	Log       *log.Logger
}

// GamesStore is a permanent storage accessor for the service's game data.
// In normal operation, GamesStore and UserStore are implemented by the same struct (like the sqlite implementation).
// However, that is optional. The main point of having 2 interfaces is trying to separate concerns and
// allowing easier mocking for testing purposes.
type GamesStore interface {
	GamesGet(limit int, offset int) ([]Game, error)
	GameGetByURL(url string) (*Game, error)
	GameInsert(game *Game) error
	GameUpdate(url string, game *Game) error
	GamesGetUpdatedBefore(timestamp time.Time, limit int) ([]Game, error)
	GameAddPricePoint(url string, pp *PricePoint) error
}

// UserStore is a permanent storage accessor for the service's user data.
// In normal operation, GamesStore and UserStore are implemented by the same struct (like the sqlite implementation).
// However, that is optional. The main point of having 2 interfaces is trying to separate concerns and
// allowing easier mocking for testing purposes.
type UserStore interface {
	UsersGetByGameURL(url string) ([]User, error)
	UserAdd(user User) error
	UserGet(userID string) (*User, error)
	UserGamesAdd(userID string, gameURL string) error
	UserGamesRemove(userID string, gameURL string) error
}

// Website is an interface to the eShop
type Website interface {
	NewGame(url string) (*Game, error)
	GetPriceFromGame(game *Game) (*PricePoint, error)
}

// NotificationService is an interface to a means to notify Users.
// In this project, it will interface with Telegram.
type NotificationService interface {
	Notify(userID string, message string) error
}

var ErrorUserNotFound = errors.New("user not found in datastore")
var ErrorGameNotFound = errors.New("game not found in datastore")
var ErrorGameURLNotAvailable = errors.New("game was not available in the store")

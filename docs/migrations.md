# Migrations

You need the [golang migrate](https://github.com/golang-migrate/migrate) binary installed to create migrations.

## Create new migration

```bash
migrate create -seq -ext sql -dir pkg/database/migrations name_of_the_migration
```

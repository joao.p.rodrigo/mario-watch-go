package main

import (
	"context"
	"fmt"
	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/database/mongo"
	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/eshop"
	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/mario"
	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/telegram"
	"log"
	"os"
)

func main() {
	fmt.Println(os.Args)
	token, ok := os.LookupEnv("TELEGRAM_BOT_TOKEN")
	if !ok {
		panic("TELEGRAM_BOT_TOKEN not set!")
	}

	mongoURL, ok := os.LookupEnv("MONGO_URL")
	if !ok {
		panic("MONGO_URL not set!")
	}
	store, err := mongo.New(mongoURL)
	if err != nil {
		log.Fatal(err)
	}

	botConfig := telegram.BotConfig{
		Token: token,
		Log:   log.Default(),
		Debug: false,
	}

	bot, err := telegram.New(botConfig)
	if err != nil {
		panic(err)
	}

	srv := mario.Service{
		GameStore: store,
		UserStore: store,
		Notifier:  bot,
		Shop:      &eshop.Website{},
		Log:       log.Default(),
	}

	bot.GameGet = srv.GameGetOrCreate
	bot.UserGamesAdd = srv.UserGamesAdd
	bot.UserGamesRemove = srv.UserGamesRemove

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go func() {
		err := bot.Start(ctx)
		if err != nil {
			panic(err)
		}
	}()

	fmt.Println("Press RETURN to stop execution")
	fmt.Scanln()
}

package main

import (
	"context"
	"fmt"
	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/database/sqlite"
	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/eshop"
	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/mario"
	"gitlab.com/joao.p.rodrigo/mario-watch-go/pkg/telegram"
	"log"
	"os"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	fmt.Println(os.Args)
	token, ok := os.LookupEnv("TELEGRAM_BOT_TOKEN")
	if !ok {
		panic("TELEGRAM_BOT_TOKEN not set!")
	}
	store, err := sqlite.NewSQLite("./main.db")
	if err != nil {
		log.Fatal(err)
	}
	err = store.UpdateDB()
	if err != nil {
		panic(err)
	}

	botConfig := telegram.BotConfig{
		Token: token,
		Log:   log.Default(),
		Debug: false,
	}

	bot, err := telegram.New(botConfig)
	if err != nil {
		panic(err)
	}

	srv := mario.Service{
		GameStore: store,
		UserStore: store,
		Notifier:  bot,
		Shop:      &eshop.Website{},
		Log:       log.Default(),
	}

	bot.GameGet = srv.GameGetOrCreate
	bot.UserGamesAdd = srv.UserGamesAdd
	bot.UserGamesRemove = srv.UserGamesRemove

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go func() {
		err := bot.Start(ctx)
		if err != nil {
			panic(err)
		}
	}()
	go runPriceUpdater(ctx, &srv, time.Second*10)
	fmt.Println("Press RETURN to stop execution")
	fmt.Scanln()
}

func runPriceUpdater(ctx context.Context, srv *mario.Service, interval time.Duration) {
	srv.WatchGames(10)
	ticker := time.NewTicker(interval)
	defer ticker.Stop()
	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			srv.WatchGames(10)
		}
	}
}
